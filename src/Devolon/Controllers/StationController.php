<?php

namespace Devolon\Http\Controllers;

use Illuminate\Http\Request;
use Devolon\Models\Station;
use App\Http\Controllers\Controller;
use Auth;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Auth::user()->company;
        return $company->stations;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = Auth::user()->company;
        $company->stations()->save(new Station(["name" => $request->name, 'latitude' => $request->latitude, 'longitude' => $request->longitude]));
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Station $station)
    {
        return $station;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Station $station)
    {
        $company = Auth::user()->company;
        $company->stations()->where('id', $station->id)->update((["name" => $request->name, 'latitude' => $request->latitude, 'longitude' => $request->longitude]));
        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Station $station)
    {
        $station->delete();
        return;
    }
}
