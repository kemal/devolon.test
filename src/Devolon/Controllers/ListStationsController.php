<?php

namespace Devolon\Http\Controllers;

use Illuminate\Http\Request;
use Devolon\Models\Station;
use Devolon\Models\Company;
use Devolon\Models\CompanyStations;
use Devolon\Requests\StationSearchRequest;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class ListStationsController extends Controller
{
    
    public function find(StationSearchRequest $request)
    {
        $calculated_distance = sprintf("( 3959 * acos( cos( radians('%s') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( latitude ) ) ) ) AS distance",
        $request->latitude, $request->longitude, $request->latitude, $request->radius);

        $stations = DB::table('stations')
            ->select('id', 'name', 'latitude', 'longitude', DB::raw($calculated_distance))
            ->havingRaw('distance < '. $request->radius)
            ->get();
        return $stations;
    }

    public function findByCompany(Company $company)
    {

        $companyStations = new CompanyStations($company);
        return $companyStations->getStations();
    }
    
}
