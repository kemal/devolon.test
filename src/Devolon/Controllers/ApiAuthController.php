<?php
namespace Devolon\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Devolon\Models\User;
use App\Http\Controllers\Controller;

class ApiAuthController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function auth(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (!Auth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (Exception $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = User::where('email', $credentials['email'])->first();
        $token = $user->createToken('Token Name')->accessToken;
        return response()->json(compact('token'));
    }
}