<?php
namespace Devolon\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name', 'parent_id'];

    public function stations()
    {
        return $this->hasMany(Station::class);
    }
}