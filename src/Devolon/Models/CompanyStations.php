<?php
namespace Devolon\Models;

class CompanyStations
{
    private $stations;

    public function __construct(Company $company)
    {
        foreach($company->stations as $station){
            $this->stations[] = $station;                
        }
        $this->getParentCompanies($company);
    }

    public function getStations()
    {
        return $this->stations;
    }

    private function getParentCompanies($company)
    {
        foreach(Company::where('parent_id', $company->id)->get() as $company){
            foreach($company->stations as $station){
                $this->stations[] = $station;                
            }
            $this->getParentCompanies($company);
        }
    }    
}