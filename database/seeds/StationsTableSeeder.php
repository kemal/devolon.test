<?php
use Devolon\Models\Company;
use Devolon\Models\Station;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StationsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Address($faker));
        

        DB::table('stations')->delete();
        $company_a = Company::where('name', 'Company A')->first();
        $company_b = Company::where('name', 'Company B')->first();
        $company_c = Company::where('name', 'Company C')->first();

        for($i = 1; $i <= 10; $i++){
            $company_a->stations()->save(new Station(["name" => "Station {$i}", 'latitude' => $faker->latitude($min = -90, $max = 90), 'longitude' => $faker->longitude($min = -90, $max = 90)]));
        }
        for($i = 11; $i <= 15; $i++){
            $company_b->stations()->save(new Station(["name" => "Station {$i}", 'latitude' => $faker->latitude($min = -90, $max = 90), 'longitude' => $faker->longitude($min = -90, $max = 90)]));
        }
        for($i = 16; $i <= 17; $i++){
            $company_c->stations()->save(new Station(["name" => "Station {$i}", 'latitude' => $faker->latitude($min = -90, $max = 90), 'longitude' => $faker->longitude($min = -90, $max = 90)]));
        }
    }
}