<?php
use Devolon\Models\Company;
use Devolon\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        $company_a = Company::where('name', 'Company A')->first();
        $company_b = Company::where('name', 'Company B')->first();
        $company_c = Company::where('name', 'Company C')->first();

        User::create(['email' => 'user1@example.com', 'password' => Hash::make('user'), 'company_id' => $company_a->id]);
        User::create(['email' => 'user2@example.com', 'password' => Hash::make('user'), 'company_id' => $company_b->id]);
        User::create(['email' => 'user3@example.com', 'password' => Hash::make('user'), 'company_id' => $company_c->id]);                
    }
}