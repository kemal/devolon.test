<?php
use Devolon\Models\Company;
use Devolon\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CompaniesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('companies')->delete();
        $company_a = Company::create(['name' => 'Company A', 'parent_id' => 0]);
        $company_b = Company::create(['name' => 'Company B', 'parent_id' =>$company_a->id]);
        Company::create(['name' => 'Company C','parent_id' => $company_b->id]);
    }
}