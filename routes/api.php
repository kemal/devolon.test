<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/auth', '\Devolon\Http\Controllers\ApiAuthController@auth');

Route::resource('stations', '\Devolon\Http\Controllers\StationController')->middleware('auth:api');

Route::post('find', '\Devolon\Http\Controllers\ListStationsController@find');

Route::get('company/{company}/stations', '\Devolon\Http\Controllers\ListStationsController@findByCompany');
